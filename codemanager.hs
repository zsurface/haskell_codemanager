{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.HashMap.Strict as M 
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import Data.List(groupBy)
--import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Monad
import Control.Monad.Trans
import Control.Concurrent 
import AronModule 
import Graphics.UI.Gtk
import Data.Text (unpack)

import Graphics.UI.Gtk.Abstract.Widget
-- import Graphics.UI.Gtk.Gdk.Events 
import Graphics.UI.Gtk
import Graphics.UI.Gtk.Gdk.DrawWindow
import Graphics.UI.Gtk.Gdk.EventM (eventKeyName)

-- | Search snippets file 
-- | Integrate it with GTK2H
-- | -------------------------------------------------------------------------------- 
-- | Compile: ghc --make codemanager.hs -o codemanager
-- | Or run: codemanager.sh
-- | -------------------------------------------------------------------------------- 
-- | dependence: $h/AronModule.hs

getInput::IO (M.HashMap String [[String]])
getInput = do
        -- let path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.hs"
        let path = "/Users/cat/myfile/bitbucket/snippets/snippet.hs"
        pplist <- readSnippet path
        let keylist = DL.map(\x -> 
                                (foldr(++) [] $ DL.map(\y -> prefix y) (fst x),
                                 snd x
                                )
                            ) pplist 
        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        let sortlist = qqsort(\x y -> f x y) lmap 
                                where f x y = fst x > fst y
        let mmap = M.fromList lmap
        let group= groupBy(\x y -> f x y) sortlist 
                              where f x y = fst x == fst y
        let uzip = map(\x -> unzip x) group
        let kmap = map(\x -> (head . fst $ x, snd x)) uzip
        let hmap = M.fromList kmap
        return hmap


-- input as key and the text as value, 
-- add text to TextBuffer
saveText :: Entry -> TextBuffer -> (M.HashMap String [[String]])-> IO ()
saveText fld txtBuf hmap = do
    key <- entryGetText fld
    case (M.lookup key hmap) of
        Just s -> textBufferSetText txtBuf (foldr(\x y -> x ++ "\n" ++ y) [] $ (map unlines s))
        _      -> textBufferSetText txtBuf "nothing" 
    return ()

fun::TextBuffer -> IO Bool 
fun txtBuf = do 
              home <- getEnv "HOME"
              let fn = home ++ "/myfile/bitbucket/haskell_webapp/uploaddir/haskelldef.txt"
              print fn 
              ls <- readFileLatin1ToList fn 
              print ls
              let str = concatStr ls "\n"
              textBufferSetText txtBuf str 
              return True

main :: IO ()
main= do
  initGUI
  sw <- scrolledWindowNew Nothing Nothing
  set sw [
          -- scrolledWindowVscrollbarPolicy := PolicyAlways,
          scrolledWindowVscrollbarPolicy := PolicyNever,
          scrolledWindowHscrollbarPolicy := PolicyAutomatic ]

  window <- windowNew
  hmap <- getInput

  vb <- vBoxNew False 0
  containerAdd window vb

  hb <- hBoxNew False 0
  boxPackStart vb hb PackNatural 0

  txtfield <- entryNew
  boxPackStart hb txtfield PackNatural 5

  -- create a vertical textView with textBuffer
  tvBuf <- textBufferNew Nothing
  textView <- textViewNewWithBuffer tvBuf
  textViewSetEditable textView True 

  containerAdd (toContainer sw) textView
--  adjustment <- adjustmentNew 0.0 0.0 101.0 0.1 1.0 1.0 
--  vp <- viewportNew adjustment adjustment
--
--  viewportSetVAdjustment vp adjustment 
--  scrolledWindowSetVAdjustment sw adjustment

  set window [windowTitle := "Code Manager", 
              windowDefaultWidth := 400,
              windowDefaultHeight := 400,
              windowWindowPosition := WinPosCenter,
              containerChild := sw, 
              containerBorderWidth := 1 
              ]

  -- boxPackStart vb textView PackNatural 2 
  boxPackStart vb sw PackNatural 2 

 
--  isPressedA <- newIORef False
--  let handler event = do
--      case event of
--          (Key released _ _ _ _ _ _ _ _ (Just char)) -> case char of
--                                                             'a' -> writeIORef isPressedA (not released)
--                                                             _ -> return ()
--          _ -> return ()
--      return True
--
--  onKeyPress window handler
--  onKeyRelease window handler

  -- widgetShowAll sw 
  widgetShowAll window

  -- Handle key press.
  -- quit window with 'q'
  window `on` keyPressEvent $ tryEvent $ do
    keyName <- eventKeyName
    liftIO $
      case unpack keyName of
        "q" -> mainQuit          -- quit
        "m" -> do
               print keyName
               return ()
  timeoutAdd (fun tvBuf) 200
  -- insert the query str to textBuffer(for the textView)
  onEntryActivate txtfield (saveText txtfield tvBuf hmap)
  onDestroy window mainQuit
  mainGUI
