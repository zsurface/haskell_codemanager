# Mon Sep 24 11:21:26 2018 
# Haskell GTK2H #

## Compile it
## ghc --make codemanager.hs -o codemanager

* Read Snippet file
* Generate prefix strings for search
* Use GTK2H for GUI, TextField, and TextView  
* Dependency: AronModule.hs

* Tue Sep 25 19:59:29 2018 
* Integrate prefix map to HTML UI
* Add new route to Wai Server
* Add some keywords coloring

* What I have learn?
- Know more about Wai API
- Know the difference between Get and Post in Html form 
- Know more about Haskell: Text, ByteString, Lazy and Strict String
- Convert String to ByteString with pack::String->ByteString
- Convert ByteString to String with unpack::ByteString->String
- Convert String to Text with pack::String->Text 
- Convert Text to String with unpack::Text->String
- Convert Strict to Lazy with Data.Text.Lazy.toStrict::Text ->Text
- Convert Lazy to Strict with Data.Text.Lazy.fromStrict::Text->Text

* TODO: fix the duplicate blocktext, e.g.  prefix = van

* Wed Jan 16 21:51:40 2019 
- Add scrollbar to textView, but there is some issue with width of viewport

 
